const Handlebars = require("handlebars");

/**
 * Comments controller
 * @url {string} - comments endpoint url (default: https://my-json-server.typicode.com/telegraph/front-end-exercise-contractors/comments)
 */
class Comments {
  /**
   * Comments constructor
   * @url {string} - comments endpoint url (default: https://my-json-server.typicode.com/telegraph/front-end-exercise-contractors/comments)
   * @return {Class}
   */
  constructor(url) {
    const defaultUrl = 'https://my-json-server.typicode.com/telegraph/front-end-exercise-contractors/comments';

    this.url = url || defaultUrl;
  }

  /**
   * fetch comments from the endpoint
   * @return {Promise<Object>}
   */
  loadComments() {
    return fetch(this.url)
      .then(response => response.json())
      .catch(err => console.log(err))
  }

  /**
   * populates comments in the DOM
   * @return {void}
   */
  populateComments(comments) {
    const formattedComments = comments.map( comment => ({...comment, dateFormatted: (new Date(comment.date)).toLocaleString()}) );
    const commentsTemplateElement = document.getElementById('comments-template');
    const commentsTemplate = Handlebars.compile(commentsTemplateElement.innerHTML)
    const commentsElement = document.getElementById('comments');
    commentsElement.innerHTML = commentsTemplate({comments: formattedComments});
    commentsElement.hidden = false;
  }

  sortByLikes(comments) {
    document.getElementById('sort-by-likes-button').classList.add('active');
    document.getElementById('sort-by-date-button').classList.remove('active');
    const sortedComments = comments.sort((a, b) => b.likes - a.likes);
    this.populateComments(sortedComments)
  }

  sortByDate(comments) {
    document.getElementById('sort-by-date-button').classList.add('active');
    document.getElementById('sort-by-likes-button').classList.remove('active');
    const sortedComments = comments.sort((a, b) => Date.parse(b.date).valueOf() - Date.parse(a.date).valueOf());
    this.populateComments(sortedComments);
  }
}

module.exports = Comments;